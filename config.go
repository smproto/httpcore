package httpcore

type Config struct {
	Timeout int `json:"timeout"`
	DefaultHeaders map[string]string `json:"defaultHeaders"`
	DefaultHost string `json:"defaultHost"`
}
