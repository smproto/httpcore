package httpcore

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func LoadResponseJson(resp *http.Response, target interface{}) error {
	return json.NewDecoder(resp.Body).Decode(target)
}

func LoadResponseString(resp *http.Response) (string, error) {
	if b, err := ioutil.ReadAll(resp.Body); err != nil {
		return "", err
	} else {
		return string(b), nil
	}
}

func UnexpectedResponseError(resp *http.Response) error {
	return fmt.Errorf("received a response with unexepcted code %v", resp.StatusCode)
}