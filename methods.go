package httpcore

const (
	Get = "GET"
	Post = "POST"
	Put = "PUT"
	Delete = "DELETE"
)