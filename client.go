package httpcore

import (
	"net/http"
	"time"
)

type Client interface {
	NewRequest(method string, url string) (*http.Request, error)
	NewJsonRequest(method string, url string, body interface{}) (*http.Request, error)
	NewStringRequest(method string, url string, body string) (*http.Request, error)
	ExecuteRequest(r *http.Request) (*http.Response, error)
}

type client struct {
	http   *http.Client
	Config *Config `apicore:"config" root:"httpClient"`
}

func NewClient() Client {
	return &client{}
}

func (client *client) OnInstall() error {
	timeout := client.Config.Timeout
	if timeout == 0 {
		timeout = 10
	}

	client.http = &http.Client {
		Timeout: time.Second * time.Duration(timeout),
	}

	return nil
}
