package httpcore

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

func (client *client) NewRequest(method string, url string) (*http.Request, error) {
	return client.buildDefaultRequest(method, url, nil)
}

func (client *client) NewJsonRequest(method string, url string, body interface{}) (*http.Request, error) {
	if b, err := json.Marshal(body); err != nil {
		return nil, err

	} else if req, err := client.buildDefaultRequest(method, url, bytes.NewReader(b)); err != nil {
		return nil, err

	} else {
		req.Header.Set("Content-Type", "application/json")
		return req, nil
	}
}

func (client *client) NewStringRequest(method string, url string, body string) (*http.Request, error) {
	if req, err := client.buildDefaultRequest(method, url, bytes.NewReader([]byte(body))); err != nil {
		return nil, err

	} else {
		req.Header.Set("Content-Type", "text/plain")
		return req, nil
	}
}

func (client *client) ExecuteRequest(req *http.Request) (*http.Response, error) {
	return client.http.Do(req)
}

func (client *client) buildDefaultRequest(method string, url string, body io.Reader) (*http.Request, error) {
	if fullUrl, err := client.processUrl(url); err != nil {
		return nil, err

	} else if req, err := http.NewRequest(method, fullUrl, body); err != nil {
		return nil, err

	} else {
		if client.Config.DefaultHeaders != nil {
			for header, value := range client.Config.DefaultHeaders {
				req.Header.Set(header, value)
			}
		}

		return req, nil
	}
}

func (client *client) processUrl(providedUrl string) (string, error) {
	if providedUrl == "" || providedUrl[0] == '/' {
		if client.Config.DefaultHost == "" {
			return "", fmt.Errorf("relative path provided but no default host is configured")

		} else if host, err := url.Parse(client.Config.DefaultHost); err != nil {
			return "", err

		} else if p, err := url.Parse(providedUrl); err != nil {
			return "", err

		} else {
			return host.ResolveReference(p).String(), err
		}
	}

	return providedUrl, nil
}