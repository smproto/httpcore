package unittest

import (
	"bitbucket.org/smproto/apicore"
	"bitbucket.org/smproto/httpcore"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"testing"
)

func TestNewRequest_RelativePathWithDefaultHost(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
			"defaultHost": "http://www.test.com",
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)

	require.NoError(t, context.Install())

	req, err := client.NewRequest(httpcore.Get, "/test")

	require.NoError(t, err)
	require.Equal(t, "http://www.test.com/test", req.URL.String())
}

func TestNewRequest_RelativePathNoDefaultHost(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)
	require.NoError(t, context.Install())

	_, err := client.NewRequest(httpcore.Get, "/test")
	require.Error(t, err)
}

func TestNewRequest_EmptyPathWithDefaultHost(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
			"defaultHost": "http://www.test.com",
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)

	require.NoError(t, context.Install())

	req, err := client.NewRequest(httpcore.Get, "")

	require.NoError(t, err)
	require.Equal(t, "http://www.test.com", req.URL.String())
}

func TestNewRequest_AbsolutePathNoDefaultHost(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)
	require.NoError(t, context.Install())

	host := "http://www.test.com"
	req, err := client.NewRequest(httpcore.Get, host)
	require.NoError(t, err)
	require.Equal(t, host, req.URL.String())
}

func TestNewRequest_AbsolutePathWithDefaultHost(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
			"defaultHost": "http://www.otherhost.com",
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)
	require.NoError(t, context.Install())

	host := "http://www.test.com"
	req, err := client.NewRequest(httpcore.Get, host)
	require.NoError(t, err)
	require.Equal(t, host, req.URL.String())
}

func TestNewRequest_DefaultHeadersAdded(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
			"defaultHeaders": map[string]interface{} {
				"Content-Type": "text/plain",
			},
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)
	require.NoError(t, context.Install())

	req, err := client.NewRequest(httpcore.Get, "http://www.test.com")
	require.NoError(t, err)
	require.Equal(t, "text/plain", req.Header.Get("Content-Type"))
}

func TestNewJsonRequest_Format(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)
	require.NoError(t, context.Install())

	body := map[string]interface{} {
		"test": "value",
	}

	req, err := client.NewJsonRequest("POST", "http://www.test.com", body)
	require.NoError(t, err)
	require.Equal(t, "application/json", req.Header.Get("Content-Type"))

	var receiver map[string]interface{}
	require.NoError(t, json.NewDecoder(req.Body).Decode(&receiver))
	require.Equal(t, body["test"], receiver["test"])
}

func TestNewStringRequest_Format(t *testing.T) {
	config := map[string]interface{} {
		"httpClient": map[string]interface{} {
		},
	}

	client := httpcore.NewClient()

	context := apicore.NewContext()
	context.LoadMapConfig(config)
	context.AddDependents(client)
	require.NoError(t, context.Install())

	body := "Test Body"

	req, err := client.NewStringRequest("POST", "http://www.test.com", body)
	require.NoError(t, err)
	require.Equal(t, "text/plain", req.Header.Get("Content-Type"))

	output, err := ioutil.ReadAll(req.Body)
	require.NoError(t, err)
	require.Equal(t, []byte(body), output)
}